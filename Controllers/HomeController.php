<?php

namespace Modules\Delivery\Controllers;

use Illuminate\Http\Request;
use Modules\Delivery\Controllers\Controller;
use Module;

class HomeController extends Controller
{
    public function __construct()
    {        
        Module::onView("content", function() {
            return "This is content view from Delivery Module HomeController";
        }, 5);
    }
    public function index(Request $request)
    {
        $message = config("delivery::app.message");
        return view('delivery::home.welcome', [
            'message' => $message,
        ]);
    }
}
