<div class="box no-border order-layout">
    <div class="position-relative box-body">
        <h4>Giao hàng</h4>
        <div>
            <a role="button" href="javascript:void(0)" class="edit-customer-btn" ng-if="!order.shipping_code" ng-click="pushOrderToShipper(order)">
                Push vận đơn
            </a>
            <a role="button" href="javascript:void(0)" class="edit-customer-btn" ng-if="order.shipping_code"  ng-click="cancelOrderToShipper(order)">
                Hủy vận đơn
            </a>

            <div class="form-group" ng-show="!order.shipping_code">
                <label for="sel-shipper">Shipper</label>
                <select name="sel-shipper" class="form-control mt-1"
                        id="sel-shipper" placeholder="Vui lòng chọn shipper"
                        ng-model="orderInfo.shipper_id"
                        ng-options="item.id as item.name for item in shippers">
                    <option value="" selected disabled="true">Đơn vị giao hàng</option>
                </select>
            </div>

            <div class="form-group" ng-show="!order.shipping_code">
                <label for="sel-warehouse">Kho</label>
                <select name="sel-warehouse" class="form-control mt-1" ng-show="!order.shipping_code"
                        id="sel-warehouse" placeholder="Vui lòng chọn shipper"
                        ng-model="orderInfo.warehouse_id">
                    <option value="">-- Kho hàng --</option>
                    <option
                            ng-disabled="!option.is_active"
                            ng-selected="orderInfo.warehouse_id == option.id"
                            ng-repeat="option in warehouses"
                            value="@{{ option.id }}">@{{ option.name }}</option>
                </select>
            </div>

            <p style="word-break: break-word; color: blue" ng-show="order.shipping_code"> Shipper:  <b>@{{ order.shipper.name }}</b></p>
            <p style="word-break: break-word; color: blue" ng-show="order.shipping_code"> Kho:  <b>@{{ order.warehouse.name }}</b></p>
            <p style="word-break: break-word" ng-if="order.shipping_code">Mã vận đơn: <b>@{{ order.shipping_code }}</b></p>
            <p style="word-break: break-word" ng-if="order.delivery_status">Trạng thái: <b>@{{ buildDeliveryStatus(order.delivery_status) }}</b></p>
        </div>
    </div>
</div>
