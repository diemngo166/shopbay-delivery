<?php

Module::onView('delivery.select-delivery-frontend', function ($data) {
    return view('delivery::includes.select-delivery');
}, 10);

Module::onView('delivery.management-in-order', function ($data) {
    return view('delivery::includes.management-in-order');
}, 10);

Module::onAction('module_loaded', function ($data) {
//    var_dump('on module_loaded: ' . json_encode($data));
}, 10);

