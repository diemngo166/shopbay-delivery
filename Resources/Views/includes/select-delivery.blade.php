<div class="row form-group" ng-show="listShippers.length > 0">
    <div class="col">
        <h2 class="tt-title-subpages noborder logistics-title">Chọn hình thức giao hàng</h2>
        <div class="logistics">
            <div class="voteBox" ng-repeat="shipper in listShippers">
                <label class="advisory" for="transfer_@{{ $index }}">
                    <input type="radio" id="transfer_@{{ $index }}" name="shipper" data-fee="@{{ shipper.fee }}" ng-model="shipperId" ng-value="shipper.id" ng-checked="$index == 0" ng-change="changeShipper()">
                    <span class="geekmark"></span>
                    <span class="transfer-name">@{{shipper.name}}</span>
                    <small>Cho phép Thanh toán khi nhận hàng</small>
                    <div class="actual-price"> @{{ moneyToString(shipper.fee) }} ₫</div>
                </label>
            </div>
        </div>
    </div>
</div>
